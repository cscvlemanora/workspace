# ----------------------------------------------------------------

                        # Git #

# ----------------------------------------------------------------

1. Version Control System
   - Why Version Control System?
   - Multiple developer working on same file/folder/project
   - Need to Merge Manually
   - a repository/database
   - History, Revert the changes etc

# ----------------------------------------------------------------

2. Version Control System Type

   - Centralized Example: SVN(subversion), Google Team Foundation Server

     1. eveone work on centralized version control
     2. if server goes down developer does not have snap/copy of repository/code

   - Distributed Example: Git,Mercurial
     1. Everyone has a copy of repository/code
     2. if server goes down developer can synch the code with each other

# ----------------------------------------------------------------

3. Why GIT?
   - Open source
   - Faster
   - Easy/Cheap to push,pull,merge etc operations
   - 90% source code use Git

# ----------------------------------------------------------------

4. Download and installation of Git
   - https://git-scm.com/download/win

# ----------------------------------------------------------------

5. Git vs GitHub vs GitLab
   - Git is version control system, It stores files or hitory in local storage when commit
   - There are many services where we an store our code on remote server like 'GitLab' and GitHub
   - Git is maintained by Linux Foundation
   - GitLab own by GitLab Inc.
   - GitHub own by Microsoft

# ----------------------------------------------------------------

6. Git Configuration
   - GitBash(Bourne Again Shell) - It is a terminal/CLI used to type commands, interface with Operating System
   - GitBash allows Unix based commands
   - pwd, ls, clear etc
   - $ git config --global user.name "amolj"
   - $ git config --global user.email "amolj@mkcl.org"
   - code .

# ----------------------------------------------------------------

7. Git init vs Git clone
   - git init - It is use to repository initialization
   - git clone - It is use to clone/copy already existing code from repository

# ----------------------------------------------------------------

8. Git init
   - git init
   - create a file in local directory

# ----------------------------------------------------------------

9. git status
   - git status - It is use to get status of repository
   - git status -s - It is use to short status of repository

# ----------------------------------------------------------------

10. A new file life cycle
    - Untracked>>>Staged>>>Commit>>Unmodified>>Modified>>Staged>>Commit
    - git add - staged the untracked a file
    - git add . OR git add -A - staged the untracked all files
    - git rm --cached "fileName" - untrack the file
    - git restore --staged "fileName" - unstaged the file
    - git rm -f "fileName" - remove the file from disk
    - git commit -m "commit message" - used to commit the staged area changes
    - git commit -a -m "commit without staged area" - used to commit the modified files directly without staged area

# ----------------------------------------------------------------

11. git checkout
    - git checkout "fileName" - It used to recovered a file if it changed/deleted by mistake
    - git checkout -f - It used to recovered all files if it changed/deleted by mistake
    - Condition: All changes should staged or commited for the respective files

# ----------------------------------------------------------------

12. git log
    - git log - It shows the all history of the commits with auther and time stamp
    - git log -p -1/2/3...n - It shows the (last 1/2/3..n) history of the commits with changes

# ----------------------------------------------------------------

13. git diff
    - git diff - It shows the diff/comaire the working directory with staged area
    - git diff --staged - It shows the diff/comaire the staged area and last commit

# ----------------------------------------------------------------

14. git ignore

- add .gitignore file in to repository to ignore the files that are not relevant/require
- "fileName" ignores the file which is matching the name. Example: sample.log, config.json
- "folderName/"" ignore complete folder. Example: logs/, node_modules/
- "_.extention" ignore complete files having extension is matched. Example: _.log, \*.js

# ----------------------------------------------------------------

15. GIT Branching

- GIT Branching is the procedure of creating the branch then merging that branch into main branch/codebase. Example: diagram git-branch
- git branch - it shows the branch list at locallay
- git branch -r - it shows the branch list at remote location
- git branch - create a new branch Example: git branch new-feature-branch
- git checkout branchName - switch to branchName. Example: git checkout new-feature-branch
- git checkout -b branchName - create and switch to a new branch. Example: git checkout -b new-feature-branch
- git branch -m newBranchName -Rename the existing branch. Example: git branch -m latest-feature-branch
- git branch -d branchName - Delete the existing branch. Example: git branch -d latest-feature-branch
- git branch -D branchName - Delete the existing forcefully branch. Example: git branch -D latest-feature-branch
- git merge branchName - It merges the branch into master branch. Example: git merge latest-feature-branch

# ----------------------------------------------------------------

16. GIT Remote

- Remote repository is a location where we can push our code
- Create an account on GitLab
- Create a Project on GitLab
- set our repository configuration as per GitLab account i.e git config --global user.email "jadhaoamoln@gmail.com" same user.name

- git remote origin URL - Set our local repository to the remote repository. Example: git remote add origin git@gitlab.com:MKCL-Group/git-demo-project.git

- git remote - It shows the remote repository
- git remote -v - Show the remote branch url where we can push and pull the code
- git push origin master -It push master branch to origin(Remote)
- Private project/Repository can not allow push
- ssh-keygen -o -t rsa -b 4096 -C "jadhaoamoln@gmail.com" - Generate a SSH key
- cd to ssh ket location then cat id_rsa.pub
- copy ssh key paste in preferces option in GitLab
- git remote set-url origin URL -If you get permission error. Example: git remote set-url origin git@gitlab.com:MKCL-Group/git-demo-project.git
- You can also change protected branch setting from GitLab>>Setting>>Repository>>Protected Branch

# ----------------------------------------------------------------

17. git pull origin master

- git pull origin master/main - it take a pull/copy from origin to master/main

# ----------------------------------------------------------------

18. Git clone

- git clone URL -It use to clone/copy code from remote to local. Example: git clone https://gitlab.com/amoljadhao/git-demo-project.git

# ----------------------------------------------------------------

19. Git conflicts

- demo

# ----------------------------------------------------------------

20. GitLens and GitBash configuration for VS code
